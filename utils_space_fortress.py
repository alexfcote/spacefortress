import os
import scipy
import numpy as np
import pandas as pd 

from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, roc_auc_score
from sklearn.model_selection import cross_val_score

def print_results(sk_class, X_train, X_test, Y_train, Y_test):
    if hasattr(sk_class, 'best_estimator_'):
        print(sk_class.best_params_)
        print('Best cv score : {:.3f}'.format(sk_class.best_score_))
        sk_class = sk_class.best_estimator_
    print(type(sk_class))
    print('train accuracy : {:.3f}'.format(accuracy_score(Y_train, sk_class.predict(X_train))))
    Y_pred = sk_class.predict(X_test)
    print_classification_report(Y_test, Y_pred)
    

def print_classification_report(Y_test, Y_pred):
    print('test accuracy : {:.3f}'.format(accuracy_score(Y_test, Y_pred)))
    print('test roc_auc_score : {:.3f}'.format(roc_auc_score(Y_test, Y_pred)))
    print(confusion_matrix(Y_test, Y_pred))
    print(classification_report(Y_test, Y_pred))
    print()

def print_cross_val_results(classifier, X_test, Y_test, cv=15):
    cv_score = cross_val_score(classifier, X_test, Y_test, cv=cv)
    print("CV Score : Mean - %.7g | Std - %.7g | Min - %.7g | Max - %.7g" % (np.mean(cv_score),np.std(cv_score),np.min(cv_score),np.max(cv_score)))

if __name__ == '__main__':
    pass