# Alexis Fortin-C�t� 2017

Ce dossier comprend le code servant � faire l'analyse des donn�es de l'�tude space fortress.
Les fichiers .ipynb sont des notebooks pour Jupyter notebook sur python 3.5. L'environnement conda SpaceFortress est utilis� pour les rouler. Les librairies install�es sont list�e dans le fichier requirements.txt
Les notebooks doivent �tre �x�cut�s dans l'ordre. expemple: 1.0, 1.1, 1.2 etc...
Les donn�es trait�s sont das le dossier Data
Le dossier Documents contients des fichiers relatif au projet
Le dossier Conf�rence HCII contient les documents relatif � l'article et la conf�rence. L'article est disponible sur google docs � l'adresse https://docs.google.com/document/d/1POul2_PwaNY7eQivYPiXDMm0ysMusH2xMvTF4ctPnf8/edit?usp=sharing

Le dossier experiment_tools contient le code qui permet de faire l'exp�rimentation. C'est un serveur web Flask qui va lire le questionnaire dans google docs et le score dans spacefortress pour prendre la d�cision de continuer ou pas.
Les images pour l'article sont contenu dans le dossier img
