from flask import Flask, render_template, session, redirect, url_for, request
import random, threading, webbrowser
import pandas as pd
import re
import os
from wtforms import Form, StringField, validators
import json
import argparse
import time


app = Flask(__name__)
app.secret_key = "asd`jfa0923qafalk;hsdf9q23rhoADFW" + str(random.randrange(100000))


with open('default_path.json', 'r') as f:
    settings = json.load(f)

SHEET_URL = settings['SHEET_URL']
SPACE_FORTRESS_PATH = settings['SPACE_FORTRESS_PATH']

id_last = 1

@app.route("/", methods=['GET', 'POST'])
def index():
    global SPACE_FORTRESS_PATH, SHEET_URL
    form = SpaceForm(request.form)
    if request.method == 'POST':
        SPACE_FORTRESS_PATH = str(form.spacepath.data)
        SHEET_URL = str(form.googlepath.data)

    form.spacepath.data = SPACE_FORTRESS_PATH
    form.googlepath.data = SHEET_URL
    form.spacepath.object_data = SPACE_FORTRESS_PATH
    form.googlepath.object_data = SHEET_URL


    if 'experience' in session.keys():
        experience = session['experience']
    else:
        experience = {'last_score':0, 'score':0, 'num_game':0}
        session['experience'] = experience

    return render_template('index.html', experience=experience, form=form)

@app.route("/read_last")
def read_last():
    try:
        par, phase, essai, surcharge, last_par, last_surcharge, last_phase, num_game = read_google_docs()
    except AttributeError:
        return "Erreur dans la réponse au questionnaire. Avez-vous bien entré le numéro du participant, la étape et l'essai? \n #par-#étape-#essai"
    score, last_score = read_space_fortress()

    global DEBUG
    if DEBUG:
        last_score = random.randint(-100, 100) + score
        last_surcharge = [2, 5][random.randint(0, 1)]


    if (last_par != par) or (last_phase!=phase):
        valid_prediction = 0
        passer_prochaine_phase = 0
    
    else:
        valid_prediction = 1
        passer_prochaine_phase = int((last_surcharge >= surcharge) and (score >= last_score))


    experience = {'participant':int(par), 
                  'phase':int(phase), 
                  'essai':int(essai), 
                  'surcharge':int(surcharge),
                  'last_surcharge':int(last_surcharge),
                  'valid_prediction':valid_prediction,
                  'passer_prochaine_phase':passer_prochaine_phase,
                  'score':score,
                  'last_score':last_score,
                  'time':time.ctime(),
                  'num_game':num_game
                  }
    session['experience'] = experience

    if not os.path.isfile('log.csv'):
        with open('log.csv', 'w') as f:
            line = ','.join([x for x in experience.keys()])
            f.write(line + '\n')

    with open('log.csv', 'a') as f:
        line = ','.join([str(x) for x in experience.values()])
        f.write(line + '\n')


    return redirect(url_for("index"))

def parse_participant(par_str):
    try:
        par = re.match("(\d+)-(\d+)-(\d+)", par_str).groups()[0]
    except:
        par = 99999
    return int(par)

def read_google_docs():
    df = pd.read_csv(SHEET_URL)
    surcharge = df.iloc[-id_last]["J'étais surchargé par cette tâche."]
    par_str = df.iloc[-id_last]["# de participant"]
    par, phase, essai = re.match("(\d+)-(\d+)-(\d+)", par_str).groups()

    last_surcharge = df.iloc[-id_last-1]["J'étais surchargé par cette tâche."]
    par_str = df.iloc[-id_last-1]["# de participant"]
    last_par, last_phase, last_essai = re.match("(\d+)-(\d+)-(\d+)", par_str).groups()

    df_phase3 = df.iloc[1651:]
    num_game = (df_phase3["# de participant"].apply(parse_participant) == int(par)).sum()

    return tuple(map(int, (par, phase, essai, surcharge, last_par, last_surcharge, last_phase, num_game)))

def read_space_fortress():
    files = os.listdir(SPACE_FORTRESS_PATH)
    files = [f for f in files if 'SpaceFortress-5.1.0_beta' in f]
    files = sorted(files)
    time_dict = {}
    
    # sort by datetime
    for file in files:
        key = re.search('(\d{4})-(\d+)-(\d+)_(\d+)-(\d+)-(\d+)', file).groups()
        key = pd.to_datetime('-'.join(key), format='%Y-%m-%d-%H-%M-%S')
        time_dict[key] = file

    last_key = sorted(time_dict.keys())[-1]
    last_file = time_dict[last_key]
    print('Last file : ' + last_file)

    while True:
        try:
            df_text = pd.read_csv(os.path.join(SPACE_FORTRESS_PATH, last_file), sep='\t')
            df_text = df_text[df_text.event_type == 'STATE'][df_text.current_game!=0]
            df_text = df_text.loc[:, ['participant', 'session', 'score_pnts', 'score_flight', 'current_game']]
            df_text['score_performance'] = df_text['score_flight'] + df_text['score_pnts']

            score = df_text['score_performance'].iloc[-2]
            current_game = df_text['current_game'].iloc[-2]
            if current_game == 1:
                last_score = 0
            else:
                last_score = df_text.loc[df_text['current_game']!=current_game, 'score_performance'].iloc[-1]
            return int(score), int(last_score)
        except Exception as e:
            print(e)






class SpaceForm(Form):
    global SPACE_FORTRESS_PATH, SHEET_URL
    spacepath = StringField('chemin Scores SpaceFortress ', 
                            [validators.Length(min=4, max=999999)], 
                            default=SPACE_FORTRESS_PATH,
                            render_kw={'style':"width: 80%"})
    googlepath = StringField('url formulaire', 
                            [validators.Length(min=4, max=999999)], 
                            default=SHEET_URL,
                            render_kw={'style':"width: 80%"})
    


if __name__ == '__main__':
    global DEBUG
    parser = argparse.ArgumentParser(description='Launch SpaceFortressHelper')
    parser.add_argument('--debug', help='Flask debug mode', action='store_true')

    args = parser.parse_args()

    DEBUG = args.debug

    if args.debug:
        port = 5000
    else:
        port = 5000
        url = "http://127.0.0.1:{0}".format(port)

        #threading.Timer(1.25, lambda: webbrowser.open(url) ).start()

    print("Connecter vous au site web : 192.168.123.248:5000")
    app.run(host='0.0.0.0', port=port, debug=args.debug, use_reloader=False)